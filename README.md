# 微信域名状态监测

#### 介绍
最近手里有个项目需要检测域名在微信里是否可以打开，如果被微信拦截，则需要进行下一步操作，所以需要判断域名的状态，但是微信官方并没有提供相关查询的方法，最后在网上找到了这个接口地址，分享给有需要的朋友。